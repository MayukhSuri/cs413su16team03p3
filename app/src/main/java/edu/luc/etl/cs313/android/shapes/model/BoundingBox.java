package edu.luc.etl.cs313.android.shapes.model;

/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {

	// DONE entirely your job (except onCircle)

	@Override
	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
	}

	@Override
	public Location onFill(final Fill f) {
		return f.getShape().accept(this);
	}

	@Override
	public Location onGroup(final Group g) {
		Integer x1 = null, y1 = null, x2 = null, y2 = null;   // Integer instead of int to use null
		Location totalBox = g.getShapes().get(0).accept(this);
		x1 = totalBox.getX();
		y1 = totalBox.getY();
		x2 = x1 + ((Rectangle) totalBox.getShape()).getWidth();
		y2 = y1 + ((Rectangle) totalBox.getShape()).getHeight();

		for (int i = 1; i < g.getShapes().size(); i++) {
			Location nextBox = g.getShapes().get(i).accept(this);
			x1 = x1 < nextBox.getX() ? x1 : nextBox.getX();
			y1 = y1 < nextBox.getY() ? y1 : nextBox.getY();
			x2 = x2 > nextBox.getX() + ((Rectangle) nextBox.getShape()).getWidth() ? x2 : nextBox.getX() + ((Rectangle) nextBox.getShape()).getWidth();
			y2 = y2 > nextBox.getY() + ((Rectangle) nextBox.getShape()).getWidth() ? y2 : nextBox.getY() + ((Rectangle) nextBox.getShape()).getHeight();
			totalBox = new Location(x1, y1, new Rectangle(x2 - x1, y2 - y1));
		}
		return totalBox;
	}

	@Override
	public Location onLocation(final Location l) {
		Location l2 = l.getShape().accept(this);
		return new Location(l.getX() + l2.getX(), l.getY() + l2.getY(), l2.getShape());   // NB: problem was location wasn't implemented properly...
	}

	@Override
	public Location onRectangle(final Rectangle r) {
		return new Location(0,0, new Rectangle(r.getWidth(), r.getHeight()));
	}

	@Override
	public Location onStroke(final Stroke c) {
		return c.getShape().accept(this);
	}

	@Override
	public Location onOutline(final Outline o) {
		return o.getShape().accept(this);   // new Location(0, 0, o.getShape());
	}

	@Override
	public Location onPolygon(final Polygon s) {
		return onGroup((Group) s);
	}
}
